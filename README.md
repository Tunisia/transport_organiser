postcode.py
============

Requirements: Python 3+

Usage:

`python transport path_to_data.tsv`

Point the script at a file of tab-separated values (e.g. data directly pasted from a google spreadsheet), and it'll print out some prospective lifts.

Data format:

TSV file rows must be of format 
`name<TAB>email_address<TAB>post_code<TAB>number_of_spaces`