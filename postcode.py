#!/usr/bin/env python

from urllib import error, request
import json
import math
import sys

csv_path = sys.argv[1]

post_codes_url = r"http://uk-postcodes.com/postcode/{}.json"

skipped_players = []
liftless_players = []

class Player():
    def __init__(self, name, post_code, spaces):
        self._valid = True
        self.name = name
        self.post_code = post_code
        self.spaces = spaces
        self.lat_long = self.get_lat_long()
        self.taking = []

    def get_lat_long(self):
        try:
            print("\tLooking up post code " + self.post_code)
            pc_no_space = ''.join(self.post_code.split(' '))
            request_url = post_codes_url.format(pc_no_space)
            data_str = request.urlopen(request_url).read().decode()

            data_dict = json.loads(data_str)

            return data_dict['geo']['lat'], data_dict['geo']['lng']
        except error.HTTPError:
            print("\t\tInvalid post code, skipping")
            self._valid = False
            skipped_players.append(self)
            return None


def get_distance(driver, passenger):
    return math.sqrt(
        abs(driver.lat_long[0] - passenger.lat_long[0])**2 + abs(driver.lat_long[1] - passenger.lat_long[1])**2)


def get_cost(driver, passenger):
    return get_distance(driver, passenger)**2


def order_by_cost(costs_dict):
    data = []
    for driver in costs_dict:
        for passenger in costs_dict[driver]:
            data.append((driver, passenger, costs_dict[driver][passenger]))

    return list(sorted(data, key=lambda x: x[2]))


def assign_by_cost(sorted_costs):
    s_c = list(sorted_costs)
    passengers = set()
    taken = set()
    while s_c:
        driver, passenger, cost = s_c[0]
        passengers.add(passenger)
        if len(driver.taking) >= driver.spaces:
            s_c.pop(0)
        elif passenger in taken:
            s_c.pop(0)
        else:
            driver.taking.append(passenger)
            taken.add(passenger)
            passengers.remove(passenger)
            s_c.pop(0)

    liftless_players = list(passengers)


players = []

print("Loading data...")
with open(csv_path, "r") as csv_file:
    for line in csv_file:
        name, _, post_code, spaces = line.split("\t")
        player = Player(name, post_code, int(spaces))
        if player._valid:
            players.append(player)

drivers = [player for player in players if player.spaces > 0]
passengers = [player for player in players if player.spaces is 0]

no_spaces = sum((driver.spaces for driver in drivers))

if no_spaces < len(passengers):
    print("WARNING: Not enough spaces!")

print("Calculating distances...")

costs = dict()
for driver in drivers:
    costs[driver] = dict()
    for passenger in passengers:
        costs[driver][passenger] = get_cost(driver, passenger)

sorted_costs = order_by_cost(costs)
assign_by_cost(sorted_costs)

print("Done!")

for driver in drivers:
    taken_names = "\n\t".join(["{} @ {}".format(passenger.name, passenger.post_code) for passenger in driver.taking])
    print("{} @ {} is taking:\n\t{}".format(driver.name, driver.post_code, taken_names))

print("\n\nNo spaces available for:")
for player in liftless_players:
    print("\t{} @ {}".format(player.name, player.post_code))

print("\n\nFailed to find:")
for player in skipped_players:
    print("\t{} @ {}".format(player.name, player.post_code))
